# Implement the following algorithm:
# Accept an integer for n
# Initialize sum to 0
# For each interger i in the range of 1 to n:
# 	Assign sum +i to sum
# Print the value of sum

CC     = gcc
CFLAGS = -g -Wall

TARGET = summation

all: $(TARGET)

summation: summation.c
	$(CC) $(CFLAGS) -o $(TARGET) summation.c

clean:
	rm $(TARGET)

